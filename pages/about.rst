---
title: About
---

- Twitter_
- Github_

This site was generated thanks to Hakyll_.
It is hosted on Gitlab Pages and you can find its repository `here<https://github.com/flupe/site>`_

.. _Hakyll: https://jaspervdj.be/hakyll
.. _Twitter: https://twitter.com/sbbls
.. _Github: https://github.com/flupe
