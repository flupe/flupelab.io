const katexMacros = {
  '\\ZZ': '\\mathbb{Z}',
  '\\T': '\\intercal',
}

document.body.querySelectorAll('.math').forEach(e => {
  let raw = e.innerText
  let content = raw.substring(2, raw.length - 2)
  let displayMode = e.className.includes("display")
  katex.render(content, e, { displayMode, macros: katexMacros })
})
