{-# LANGUAGE OverloadedStrings #-}

import Hakyll

import Text.Pandoc.Options
import Text.Pandoc.Definition

import Data.Map.Strict (foldMapWithKey, mapMaybeWithKey)
import Data.Functor ((<&>))
import Data.Function ((&))


config :: Configuration
config = defaultConfiguration
    { destinationDirectory = "public"
    }


main :: IO ()
main = hakyllWith config $ do

    match ("images/*" .||. "js/*") $ do
        route   idRoute
        compile copyFileCompiler


    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler


    match "pages/*" $ do
        route $ (gsubRoute "pages/" (const "")) `composeRoutes` setExtension "html"

        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/page.html"    defaultContext
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls


    match "posts/*" $ do
        route $ setExtension "html"

        let
            ropts = defaultHakyllReaderOptions
                { readerStandalone = True
                }

            wopts = defaultHakyllWriterOptions
                { writerHTMLMathMethod  = KaTeX ""
                , writerSectionDivs     = True
                , writerTableOfContents = True
                , writerTemplate        = Just "<nav class=\"toc\">$toc$</nav>\n$body$"
                }

        compile $ do
            document <- getResourceBody >>= readPandocWith ropts

            let
                Pandoc meta _ = itemBody document

                inlinesToString :: [Inline] -> String
                inlinesToString inlines =
                    concatMap inlineToString inlines
                    where
                        inlineToString (Str a) = a
                        inlineToString (Space) = " "

                extractMeta :: String -> MetaValue -> Context a
                extractMeta name metavalue =
                    case metavalue of
                        MetaInlines inlines -> mkField $ inlinesToString inlines
                        _ -> mempty
                    where
                        mkField = field name . const . return

                extractMeta' :: String -> MetaValue -> Maybe String
                extractMeta' name (MetaInlines inlines) = Just (inlinesToString inlines)
                extractMeta' name _                     = Nothing

                ctx :: Context String
                ctx = foldMapWithKey extractMeta (unMeta meta)
                    <> postCtx

            makeItem (mapMaybeWithKey extractMeta' (unMeta meta))
                >>= saveSnapshot "context"

            writePandocWith wopts document
                &   loadAndApplyTemplate "templates/post.html" ctx
                >>= saveSnapshot ""
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls


    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"

            let archiveCtx =
                    listField "posts" postCtx (return posts)
                    <> constField "title" "Archives"
                    <> defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/page.html"    archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls


    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"

            let indexCtx =
                    listField "posts" postCtx (return $ take 5 posts)
                    <> constField "title" "Home"
                    <> defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/page.html"    indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls


    match "templates/*" $ compile templateBodyCompiler


postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y"
    <> dateField "datetime" "%F"
    <> defaultContext
